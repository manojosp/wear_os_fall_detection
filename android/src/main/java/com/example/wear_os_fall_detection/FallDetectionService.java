package com.example.wear_os_fall_detection;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;


import java.text.DecimalFormat;


public class FallDetectionService extends Service implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private PowerManager.WakeLock wakeLock;

    public FallDetectionService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("onCreate", "onCreate method called");
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        } else {
            Toast.makeText(this, "No Accelerometer Found!!",
                    Toast.LENGTH_LONG).show();
        }
        PowerManager pm = (PowerManager)getSystemService(POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "FallDetectionService:Wakelock");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("onBind", "onBind method called");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("onStartCommand", "onStartCommand method called");
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        wakeLock.acquire();
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        Log.d("onDestroy", "onDestroy method called");
        sensorManager.unregisterListener(this);
        Log.d("onDestroy", "onDestroy method call completed");
        wakeLock.release();
        super.onDestroy();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            double loX = event.values[0];
            double loY = event.values[1];
            double loZ = event.values[2];
            double loAccelerationReader = Math.sqrt(Math.pow(loX, 2)
                    + Math.pow(loY, 2)
                    + Math.pow(loZ, 2));
            DecimalFormat precision = new DecimalFormat("0.00");
            double ldAccRound = Double.parseDouble(precision.format(loAccelerationReader));
            Log.e("onSensorChanged", "onSensorChanged: " + ldAccRound + " X=" + loX + " Y=" + loY + " Z=" + loZ);
            if (ldAccRound > 0.3d && ldAccRound < 0.5d) {
                Toast.makeText(this, "FALL DETECTED!!!!!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}