package com.example.wear_os_fall_detection;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;


import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * WearOsFallDetectionPlugin
 */
public class WearOsFallDetectionPlugin implements FlutterPlugin, MethodCallHandler {//, ActivityAware
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;
    private Context context;
    //private Activity activity;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        Log.d("onAttachedToEngine", "onAttachedToEngine method called");
        this.context = flutterPluginBinding.getApplicationContext();
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "wear_os_fall_detection");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("startWearOsFallDetection")) {
            ///TODO add start wear os fall detection logic here
            Log.d("startFallDetection", "startWearOsFallDetection method called");
            this.context.startService(new Intent(this.context, FallDetectionService.class));
            result.success(null);
        } else if (call.method.equals("stopWearOsFallDetection")) {
            ///TODO add stop wear os fall detection logic here
            Log.d("stopFallDetection", "stopWearOsFallDetection method called");
            this.context.stopService(new Intent(this.context, FallDetectionService.class));
            result.success(null);
        } else {
            result.notImplemented();
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        Log.d("onDetachedFromEngine", "onDetachedFromEngine method called");
        this.context = null;
        channel.setMethodCallHandler(null);
    }
}
