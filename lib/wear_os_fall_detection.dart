import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class WearOsFallDetection {
  static const MethodChannel _channel = MethodChannel('wear_os_fall_detection');

  static Future<void> startWearOsFallDetectionService() async {
    try {
      await _channel.invokeMethod('startWearOsFallDetection');
    } on PlatformException catch (err) {
      if (kDebugMode) {
        print(err);
      }
    } catch (err) {
      if (kDebugMode) {
        print(err);
      }
    }
  }

  static Future<void> stopWearOsFallDetectionService() async {
    try {
      await _channel.invokeMethod('stopWearOsFallDetection');
    } on PlatformException catch (err) {
      if (kDebugMode) {
        print(err);
      }
      // Handle err
    } catch (err) {
      if (kDebugMode) {
        print(err);
      }
      // other types of Exceptions
    }
  }
}
