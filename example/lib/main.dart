import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:wear_os_fall_detection/wear_os_fall_detection.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  //final methodChannel = const MethodChannel("wear_os_fall_detection");

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    await WearOsFallDetection.stopWearOsFallDetectionService();
    await Future.delayed(const Duration(seconds: 10));
    await WearOsFallDetection.startWearOsFallDetectionService();
    //await methodChannel.invokeMethod("startWearOsFallDetection");
    await Future.delayed(const Duration(seconds: 10));
    //await methodChannel.invokeMethod("stopWearOsFallDetection");
    await WearOsFallDetection.stopWearOsFallDetectionService();
  }

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: Text('Running'),
        ),
      ),
    );
  }
}
